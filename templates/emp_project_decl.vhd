-- emp_project_decl
--
-- Defines constants for the whole device

library ieee;
use ieee.std_logic_1164.all;

library lpgbt_lib;
use lpgbt_lib.lpgbtfpga_package.all;

use work.emp_framework_decl.all;
use work.emp_device_decl.all;
use work.emp_device_types.all;
use work.emp_lpgbt_decl.all;
use work.emp_data_framer_decl.all;
use work.emp_slink_types.all;


-------------------------------------------------------------------------------
package emp_project_decl is

    constant PAYLOAD_REV         : std_logic_vector(31 downto 0) := X"D7C00002";

    -- Latency buffer size
    constant LB_ADDR_WIDTH      : integer               := 10;

    -- Clock setup
    constant CLOCK_COMMON_RATIO : integer               := 36;
    constant CLOCK_RATIO        : integer               := 9;
    constant CLOCK_AUX_DIV      : clock_divisor_array_t := (18, 9, 4); -- Dividers of CLOCK_COMMON_RATIO * 40 MHz


    -- Only used by nullalgo
    constant PAYLOAD_LATENCY    : integer             := 5;

    -- mgt -> buf -> fmt -> (algo) -> (fmt) -> buf -> mgt -> clk -> altclk
    {{ REGION_CONF }}

    -- for data framer (ic_simple, no_ec, n_ec_spare, ec_broadcast)
    {{ REGION_DATA_FRAMER_CONF }}

    -- for lpgbt
    {{ REGION_LPGBT_CONF }}

    -- Specify the slink quad using the corresponding region conf ID
    -- Specify slink channels to enable using the channel mask
    constant SLINK_CONF : slink_conf_array_t := (
        others => kNoSlink
    ); 



end emp_project_decl;
-------------------------------------------------------------------------------
