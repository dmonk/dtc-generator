from typing import Any


def generateConstant(name: str, vhdl_type: str, value: Any, indentation: int = 0) -> str:
    if type(value) is not list:
        s = "" # ("").join([" " for _ in range(indentation)])
        s += f"constant {name} : {vhdl_type} := {value};"
        return s
    else:
        s = "" # ("").join([" " for _ in range(indentation)])
        s += f"constant {name} : {vhdl_type} := (\n"
        for i, entry in enumerate(value):
            s+= ("").join([" " for _ in range(indentation)]) + ("").join([" " for _ in range(4)])
            s+= entry
            if i != len(value) - 1:
                s += ",\n"
            else:
                s += "\n"
        s += ("").join([" " for _ in range(indentation)]) + ");"
        return s