import requests
from typing import Dict, Any, List
from yaml import safe_load
import sys

from dtcgenerator.files import DTCLinkMap, EMPProjectDecl
from dtcgenerator.link import Link


def main() -> None:
    filename = sys.argv[1]
    with open(filename) as f:
        build_configs = safe_load(f)
    print(build_configs)
    modules = build_configs["modules"]
    sorted_modules = sorted(modules, key=lambda d: d['channels']['dtc'])
    link_map = DTCLinkMap([Link(module) for module in sorted_modules], build_configs["templates"])
    link_map.save(f"generated-files/{build_configs['algorithm']}/firmware/hdl/link_maps.vhd")
    
    emp_project_decl = EMPProjectDecl([Link(module) for module in sorted_modules], build_configs["templates"])
    emp_project_decl.save(f"generated-files/{build_configs['algorithm']}/firmware/hdl/emp_project_decl.vhd")


if __name__ == "__main__":
    main()