from typing import Dict, Any

class Link:
    def __init__(self, config: Dict[str, Any]) -> None:
        self.config = config
        
    def generateLinkMapLine(self) -> str:
        line = f"{str(self.config['channels']['dtc']):3} => ({self.config['channels']['emp']}, \"{self.config['type']}\", {self.config['bandwidth']}, \"CIC{self.config['cic_version']}\")"
        return line
