from typing import List
import jinja2
import pathlib

from .func import generateConstant
from .link import Link


class ConfigFile:
    def __init__(self, links: List[Link], template_path: str = "templates/") -> None:
        self.links = links
        self.template_path = template_path
    
    def save(self, filename: str = "config.vhd") -> None:
        s = self.generate()
        file = pathlib.Path(filename)
        file.parent.mkdir(parents=True, exist_ok=True)
        with open(filename, "w") as f:
            f.write(s)


class DTCLinkMap(ConfigFile): 
    def generate(self) -> str:
        environment = jinja2.Environment(loader=jinja2.FileSystemLoader(self.template_path))
        template = environment.get_template("link_maps.vhd")
        return template.render(
            cNumberOfFEModules=generateConstant('cNumberOfFEModules', 'integer', len(self.links), 4),
            cDTCInputLinkMap=generateConstant('cDTCInputLinkMap', 'tDTCInputLinkMap', [link.generateLinkMapLine() for link in self.links], 4),
            cNumberOfOutputLinks=generateConstant('cNumberOfOutputLinks', 'integer', len(self.links), 4),
            cDTCOutputLinkMap=generateConstant('cDTCOutputLinkMap', 'tDTCOutputLinkMap', [f"{12+i}" for i in range(len(self.links))], 4)
        )


class EMPProjectDecl(ConfigFile):
    def _generateLPGBTQuad(self, quad: int) -> str:
        return f"{str(quad):6} => (lpgbt, buf, no_fmt, buf, lpgbt)"
    
    def _generateDataFramerConfLine(self, quad: int, indentation: int) -> str:
        default = "(false, true, 0, false, lpgbtv1)"
        channels = [default for i in range(4)]
        for link in self.links:
            if link.config['channels']['emp']//4 == quad:
                channel = link.config['channels']['emp'] % 4
                channels[channel] = f"(false, true, 0, false, lpgbtv{link.config['lpgbt_version']})"
        s = f"{str(quad):6} => (\n"
        for i in range(4):
            s+= ("").join([" " for _ in range(indentation)]) + ("").join([" " for _ in range(4)])
            s+= channels[i]
            if i != 3:
                s += ",\n"
            else:
                s += "\n"
        s += ("").join([" " for _ in range(indentation)]) + ")"
        return s
    
    def _generateLPGBTConfLine(self, quad: int) -> str:
        bandwidths = []
        quad_channels = [i + 4*quad for i in range(4)]
        for channel in quad_channels:
            for link in self.links:
                if channel == link.config['channels']['emp']:
                    bandwidths.append(link.config["bandwidth"])
                    break
        if len(set(bandwidths)) != 1:
            raise Exception("All channels in a quad must have the same bandwidth.")
        if bandwidths[0] == 5:
            return f"{str(quad):6} => (FEC5,  DATARATE_5G12, PCS)"
        elif bandwidths[0] == 10:
            return f"{str(quad):6} => (FEC5, DATARATE_10G24, PCS)"
        
    def generate(self) -> str:
        environment = jinja2.Environment(loader=jinja2.FileSystemLoader("templates/"))
        template = environment.get_template("emp_project_decl.vhd")
        emp_ids = [link.config['channels']['emp'] for link in self.links]
        quads = set([id//4 for id in emp_ids])
        entries = [self._generateLPGBTQuad(quad) for quad in sorted(quads)]
        for i in [1, 2, 3]:
            if i not in quads:
                entries.append(f"{str(i):6} => (gty25, buf, no_fmt, buf, gty25)")
        entries.append("others => kDummyRegion")
        return template.render(
            REGION_CONF=generateConstant(
                'REGION_CONF',
                'region_conf_array_t',
                entries,
                4
            ),
            REGION_DATA_FRAMER_CONF=generateConstant(
                'REGION_DATA_FRAMER_CONF',
                'region_data_framer_conf_array_t',
                [self._generateDataFramerConfLine(quad, 8) for quad in sorted(quads)] + ["others => kDummyRegionDataFramer"],
                4
            ),
            REGION_LPGBT_CONF=generateConstant(
                'REGION_LPGBT_CONF',
                'region_lpgbt_conf_array_t',
                [self._generateLPGBTConfLine(quad) for quad in sorted(quads)] + ["others => kDummyRegionLpgbt"],
                4
            )
        )
